export function filterQuery(filters) {
  let queries = [];
  for (const key in filters) {
    if (filters[key] != (null || "")) {
      let query = `filter[${key}]=${filters[key]}`;
      queries.push(query);
    }
  }
  return queries.join("&");
}

export function sortQuery(key, direction) {
  if (key == null) {
    return "";
  }
  return `sort=${direction == "DESC" ? "-" : ""}${key}`;
}
